6.1) Se intenta hacer override de un campo que no es declarado virtual
6.2) Se declara un método abstracto en una clase no abstracta
6.3) Se declara implementación en un método abstracto
6.4) En la clase A se hace un override de un método que no existe
6.5) Se cambia el modificador de acceso del método a sobreescribir
6.6) No se puede hacer virtual un método estático
6.7) Ambos modificadores de acceso por defecto son privados y eso causa error
  Básicamente un método virtual, con la idea de ser sobreescrito, no puede ser privado
6.8) No se utiliza el constructor de la super clase
6.9) Se intenta acceder directamente a un campo privado de la super clase
6.10) NO se puede asignar un modificador de acceso mas permisivo que el contexto donde fue declarado
  (Propiedad privada, get público)
6.11) Para que el override esté completo debe especificarse que hacer en caso de un set
6.12) Se hace override de una propiedad no virtual
