﻿// See https://aka.ms/new-console-template for more information
/* Sin borrar ni modificar ninguna línea, completar la definición de las clases B , C y D */

A[] vector = new A[] { new A(3), new B(5), new C(15), new D(41) };
foreach (A a in vector)
{
  a.Imprimir();
}
class A
{
  protected int _id;
  public A(int id) => _id = id;
  public virtual void Imprimir() => Console.WriteLine($"A_{_id}");
}
class B : A
{
  public override void Imprimir()
  {
    Console.Write($"B_{base._id} => ");
    base.Imprimir();
  }
  public B(int id) : base(id) { }
}
class C : B
{
  public override void Imprimir()
  {
    Console.Write($"C_{base._id} => ");
    base.Imprimir();
  }
  public C(int id) : base(id) { }
}
class D : C
{
  public D(int id) : base(id) { }
  public override void Imprimir()
  {
    Console.Write($"D_{base._id} => ");
    base.Imprimir();
  }
}
