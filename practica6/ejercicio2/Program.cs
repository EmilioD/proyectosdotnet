﻿/* Aunque consultar en el código por el tipo de un objeto indica habitualmente un diseño ineficiente,
por motivos didácticos vamos a utilizarlo. Completar el siguiente código, que utiliza las clases
definidas en el ejercicio anterior, para que se produzca la salida indicada: */
/* Es decir, se deben imprimir sólo los objetos cuyo tipo exacto sea B
a) Utilizando el operador is
b) Utilizando el método GetType() y el operador typeof() (investigar sobre éste último en
la documentación en línea de .net) */

A[] vector = new A[] { new C(1), new D(2), new B(3), new D(4), new B(5) };
foreach (A a in vector)
{
  if (a is B obj) obj.Imprimir();
}
/* foreach (A a in vector)
{
  if (a.GetType().Equals(typeof(B))) a.Imprimir();
} */
class A
{
  protected int _id;
  public A(int id) => _id = id;
  public virtual void Imprimir() => Console.WriteLine($"A_{_id}");
}
class B : A
{
  public override void Imprimir()
  {
    Console.Write($"B_{base._id} => ");
    base.Imprimir();
  }
  public B(int id) : base(id) { }
}
class C : B
{
  public override void Imprimir()
  {
    Console.Write($"C_{base._id} => ");
    base.Imprimir();
  }
  public C(int id) : base(id) { }
}
class D : C
{
  public D(int id) : base(id) { }
  public override void Imprimir()
  {
    Console.Write($"D_{base._id} => ");
    base.Imprimir();
  }
}

