﻿// See https://aka.ms/new-console-template for more information
/* Ofrecer una implementación polimórfica para mejorar el siguiente programa: */

Imprimidor.Imprimir(new A(), new B(), new C(), new D());

class Imprimidora
{
  public virtual void Imprimir() => Console.Write("Soy una instancia ");
}
class A : Imprimidora
{
  public override void Imprimir() { base.Imprimir(); Console.WriteLine("A"); }
}
class B : Imprimidora
{
  public override void Imprimir() { base.Imprimir(); Console.WriteLine("B"); }
}
class C  : Imprimidora
{
  public override void Imprimir() { base.Imprimir(); Console.WriteLine("C"); }
}
class D  : Imprimidora
{
  public override void Imprimir() { base.Imprimir(); Console.WriteLine("D"); }
}
static class Imprimidor
{
  public static void Imprimir(params object[] vector)
  {
    foreach (Imprimidora o in vector)
    {
      o.Imprimir();
    }
  }
}
