namespace ejercicio8;

class Vendedor : Empleado
{
  public int Comision { get; set; }
  public override double Salario { get; }
  public Vendedor(string? name, int dni, DateTime ingreso, double salario_base) : base(name, dni, ingreso, salario_base) => Salario = salario_base + Comision;

  public override void AumentarSalario(){
    SalarioBase = DateTime.Now.Year - FechaDeIngreso.Year < 10 ? SalarioBase * 1.05 : SalarioBase * 1.1;
    
    /* Salario = SalarioBase + Comision; */
  }

  public override string? ToString()
  {
    return $"Vendedor Nombre: {Nombre}, DNI: {DNI}, Antiguedad: {DateTime.Now.Year - FechaDeIngreso.Year} \n Salario base: {SalarioBase}, Salario: {SalarioBase + Comision} \n -------------";
  }
}