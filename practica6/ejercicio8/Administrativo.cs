namespace ejercicio8;

class Administrativo : Empleado
{

  public int Premio { get; set; }
  public override double Salario { get; }
  public Administrativo(string? name, int dni, DateTime ingreso, double salario_base) : base(name, dni, ingreso, salario_base) => Salario = salario_base + Premio;

  public override void AumentarSalario()
    => SalarioBase *= (DateTime.Now.Year - FechaDeIngreso.Year) / 100 + 1;

  public override string? ToString()
  {
    return $"Administrativo Nombre: {Nombre}, DNI: {DNI}, Antiguedad: {DateTime.Now.Year - FechaDeIngreso.Year} \n Salario base: {SalarioBase}, Salario: {SalarioBase + Premio} \n -------------";
  }
}