namespace ejercicio8;

abstract class Empleado
{
  public string? Nombre { get; }
  public int DNI { get; }
  public DateTime FechaDeIngreso { get; set; }

  public double SalarioBase { get; protected set; }

  public abstract double Salario { get; }

  public Empleado(string? name, int dni, DateTime ingreso, double salario_base)
  {
    Nombre = name;
    DNI = dni;
    FechaDeIngreso = ingreso;
    SalarioBase = salario_base;
  }

  public abstract void AumentarSalario();

  public abstract override string? ToString();
}