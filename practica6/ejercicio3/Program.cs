﻿// See https://aka.ms/new-console-template for more information
class Auto
{
  double velocidad;
  public virtual void Acelerar()
  => Console.WriteLine("Velocidad = {0}", velocidad += 10);
}
class Taxi : Auto
{
  public override void Acelerar()
  => Console.WriteLine("Velocidad = {0}", velocidad += 5);
}

/* El error está en que el campo sin modificador de acceso por defecto es private*/