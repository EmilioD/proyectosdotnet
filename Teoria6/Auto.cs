namespace Teoria6;

class Auto : Automotor
{
  public TipoAuto Tipo;

  public Auto(string marca, int modelo, TipoAuto tipo) : base(marca, modelo)
  {
    this.Tipo = tipo;
  }
  public override void Imprimir()
  {
    Console.Write($"Auto {Tipo} ");
    base.Imprimir();
  }

  public override void HacerMantenimiento() { }
  public override DateTime FechaService { get; set; }
  public override double PrecioDeVenta { get; }
}