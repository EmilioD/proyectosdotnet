namespace Teoria6;

class Colectivo : Automotor
{
  public int CantPasajeros;
  public override int Modelo { get => base.Modelo; protected set => base.Modelo = (value < 2015) ? 2015 : value; }

  public Colectivo(string marca, int modelo, int cant) : base(marca, modelo)
  {
    this.CantPasajeros = cant;
  }
  public override void Imprimir()
      => Console.WriteLine($"{Marca} {Modelo} ({CantPasajeros} pasajeros)");

  public override void HacerMantenimiento() { }
  public override DateTime FechaService { get; set; }
  public override double PrecioDeVenta { get; }
}