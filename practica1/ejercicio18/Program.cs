﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 18");
for (int i = 0; i <= 4; i++)
{
  string st = i < 3 ? i == 2 ? "dos" : i == 1 ? "uno" : "< 1" : i < 4 ? "tres" : "> 3";
  Console.WriteLine(st);

  /* Basicamente hace esto pero mas bonito: 

  if (i < 3)
  {
    if (i == 2){
      st = "dos";
    }else
    {
      if (i == 1)
      {
        st = "uno";
      }else
      {
        st = "< 1";
      }
    }
  }else
  {
    if (i < 4){
      st = "tres";
    }else
    {
      st = "> 3";
    }
  }
  */
}