﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 9");

string? st = Console.ReadLine();

if (st != null){
  string?[] subs = st.Split(' ');
  string? st_inv = Reverse(subs[1].ToLower());
  Console.WriteLine(st_inv.Equals(subs[0].ToLower()) ? "Son simétricas" : "No son simétricas");
}


static string Reverse( string s )
{
    char[] charArray = s.ToCharArray();
    Array.Reverse( charArray );
    return new string( charArray );
}

