﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Secuencias de escape");
Console.WriteLine();

/* 
  Escape Sequence 	Represents
  \a 	              Bell (alert)
  \b 	              Backspace
  \f 	              Form feed
  \n 	              New line
  \r 	              Carriage return
  \t 	              Horizontal tab
  \v 	              Vertical tab
  \' 	              Single quotation mark
  \" 	              Double quotation mark
  \\ 	              Backslash
  \? 	              Literal question mark
  \ ooo 	          ASCII character in octal notation
  \x hh 	          ASCII character in hexadecimal notation
  \x hhhh           Unicode character in hexadecimal notation if this escape sequence is used in a wide-character constant or a Unicode string literal.
                    For example, WCHAR f = L'\x4e00' or WCHAR b[] = L"The Chinese character for one is \x4e00".
 */

Console.Write("Ejemplo\n");
Console.Write("usando\n");
Console.Write("la\n");
Console.WriteLine("secuencia n ");

Console.WriteLine("");

Console.Write("Ejemplo\t");
Console.Write("usando\t");
Console.Write("la\t");
Console.WriteLine("secuencia t ");

Console.WriteLine("");

Console.WriteLine("Secuencia \"de\" escape");

Console.WriteLine("");

Console.WriteLine("Secuencia \\ de \\ escape");