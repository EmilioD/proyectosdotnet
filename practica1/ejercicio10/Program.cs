﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 10");

int? diez_siete = 17;
int? vent_nueve = 29;

for (int i = 1; i <= 1000; i++)
{
  if ((diez_siete%i) == 0)
  {
    Console.WriteLine(i + " es múltiplo de 17");
  }
  if ((vent_nueve%i) == 0)
  {
    Console.WriteLine(i + " es múltiplo de 29");
  }
}