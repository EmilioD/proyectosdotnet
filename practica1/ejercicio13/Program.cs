﻿// See https://aka.ms/new-console-template for more information
/* Escribir un programa que calcule la suma de dos números reales introducidos por teclado y
muestre el resultado en la consola (utilizar double.Parse(st) para obtener el valor double a
partir del string st. */

Console.WriteLine("Práctica 1 - Ejercicio 13");
Console.WriteLine("Ingrese el primer número (con coma)");
double? first = double.Parse(Console.ReadLine());
Console.WriteLine("Ingrese el segundo número (con coma)");
double? sec = double.Parse(Console.ReadLine());
Console.WriteLine("La suma es: " + (first + sec));