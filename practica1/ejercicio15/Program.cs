﻿// See https://aka.ms/new-console-template for more information
/* Escribir un programa que solicite un año por pantalla y diga si es bisiesto. Un año es bisiesto si
es divisible por 4 pero no por 100. Si es divisible por 100, para ser bisiesto debe ser divisible
por 400. */
Console.WriteLine("Práctica 1 - Ejercicio 15");

Console.WriteLine("Ingrese un año");
int? year = int.Parse(Console.ReadLine());

if ((year % 4) == 0 && (year % 100) != 0)
{
  Console.WriteLine("Es bisiesto");
}else
{
  if ((year % 100) == 0 && (year % 100) == 0)
  {
    Console.WriteLine("Es bisiesto");
  }else
  {
    Console.WriteLine("No es bisiesto");
  }
}