﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
/* 
int a, b, c; Si
int a; int b; int c, d; Si
int a = 1; int b = 2; int c = 3; Si
int b; int c; int a = b = c = 1; Si
int c; int a, b = c = 1; Si
int c; int a = 2, b = c = 1; Si
int a = 2, b, c, d = 2 * a; Si
int a = 2, int b = 3, int c = 4; Si
int a = 2; b = 3; c = 4; No
int a; int c = a; No
char c = 'A', string st = "Hola"; No
char c = 'A'; string st = "Hola"; Si
char c = 'A', st = "Hola";  No
*/