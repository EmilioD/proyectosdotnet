﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 21");
/* Analizar el siguiente código. ¿Cuál es la salida por consola? */

int i = 1;
if (--i == 0)
{
  Console.WriteLine("cero");
}
if (i++ == 0)
{
  Console.WriteLine("cero");
}
Console.WriteLine(i);



/* Va a dar cero, ya que el pre-decremento, primero decrementa y luego opera */