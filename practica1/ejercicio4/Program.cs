﻿// See https://aka.ms/new-console-template for more information

try
{
  string? st;
  if ((st = Console.ReadLine()) != null){
    Console.WriteLine(st.Equals(" ") ? "Hola, mundo!" : "Hola, " + st);  
  }
}
catch (IOException e)
{
  throw e;
}