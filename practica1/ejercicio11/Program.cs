﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 11");

Console.WriteLine("10/3 = " + 10 / 3);
Console.WriteLine("10.0/3 = " + 10.0 / 3);
Console.WriteLine("10/3.0 = " + 10 / 3.0);
int a = 10, b = 3;
Console.WriteLine("Si a y b son variables enteras, si a=10 y b=3");
Console.WriteLine("entonces a/b = " + a / b);
double c = 3;
Console.WriteLine("Si c es una variable double, c=3");
Console.WriteLine("entonces a/c = " + a / c);

/* 
Si ambos parámetros son enteros, entonces la división es entera
Si detecta que al menos uno es decimal, la división es con coma
Si ambas variables son declaradas enteras entonces la división es entera
Si detecta que al menos uno es decimal, la división es con coma por mas que una sea declarada entera*/

/*
El operador + entre u String y un valor número, lo imprime como un string, y si hay un operador, realiza la operación e imprime el resultado 
*/