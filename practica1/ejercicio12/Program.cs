﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 12");

string? st;

Console.Write("Ingrese un número: ");
st = Console.ReadLine();

int val = int.Parse(st);

for (int i = 1; i <= val; i++)
{
  if ( (val%i) == 0 )
  {
    Console.WriteLine( i + " es múltiplo de " + val);
  }
}
