﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Práctica 1 - Ejercicio 16");
int? a, b;
a = int.Parse(Console.ReadLine());
b = int.Parse(Console.ReadLine());
if ((b != 0) & (a/b > 5)) Console.WriteLine(a/b);

/* El problema radica en que se realiza la condición con el operador AND bitwise
Lo que hace es comparar una y otra condición, la cual evalúa ambas condiciones sin importar si una ya dió falsa
En cambio el operador lógico AND &&, basta con que una de las dos ya de falso para dejar de comparar
En este caso, si se ingresa b = 0, y se usa &, verifica que la primer condición da falso
pero al querer verificar la segunda falla por una excepción del sistema.
En cambio si se ingresa b = 0 y se usa &&, la primer condición ya anula todas las demas al dar false */
