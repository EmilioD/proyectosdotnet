﻿// See https://aka.ms/new-console-template for more information
/* Escribir un programa que multiplique por 365 el número entero ingresado por el usuario desde
la consola. El resultado debe imprimirse en la consola dígito por dígito, separado por un espacio
comenzando por el dígito menos significativo al más significativo. */
Console.WriteLine("Práctica 1 - Ejercicio 14");

Console.WriteLine("Ingrese un número ");
double? val = double.Parse(Console.ReadLine());
val *= 365;
//Convierte al entero en string y luego lo pasa a un arreglo
int[] arr = val.ToString().Select(o => Convert.ToInt32(o) - 48).ToArray();

foreach (var item in arr)
{
  Console.Write(item + " ");
}