﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

/* 
Ejercicio 4
try
{
  string? st;
  if ((st = Console.ReadLine()) != null){
    Console.WriteLine(st.Equals(" ") ? "Hola, mundo!" : "Hola, " + st);  
  }
}
catch (IOException e)
{
  throw e;
}
*/

string? st;
Console.WriteLine("Ingrese algún texto: [' ', Juan, María, Alberto, algún nombre] ");
if ((st = Console.ReadLine()) != null){

  Console.WriteLine("Usando if ... else if");

  if (st.Equals(string.Empty))
  {
    Console.WriteLine("Buen día mundo");  
  }else
  {
    if (st.Equals("Juan")){
      Console.WriteLine("Hola amigo!");
    }else
    {
      if (st.Equals("Maria"))
      {
        Console.WriteLine("Buen día señora");
      }else
      {
        if (st.Equals("Alberto"))
        {
          Console.WriteLine("Hola Alberto");
        }else
        {
          Console.WriteLine("Buen día " + st);
        }
      }
    }
  }

  Console.WriteLine("Usando switch");

  switch(st) 
  {
    case "":
      Console.WriteLine("Buen día mundo");  
      break;
    case "Juan":
      Console.WriteLine("Hola amigo!");
      break;
    case "Maria":
      Console.WriteLine("Buen día señora");
      break;
    case "Alberto":
      Console.WriteLine("Hola Alberto");
      break;
    default:
      Console.WriteLine("Buen día " + st);
      break;
  }


}
