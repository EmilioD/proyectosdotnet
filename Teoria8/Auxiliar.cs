namespace Teoria8;

class Auxiliar
{
  public void Procesar()
  {
    Action a;
    a = Metodo1;
    a = a + Metodo2;
    a += () => Console.WriteLine("Expresión lambda");
    a -= Metodo2;
    a?.Invoke();
    /* Delegate[] encolados = a.GetInvocationList();
    for (int i = encolados.Length - 1; i >= 0; i--)
    {
      (encolados[i] as Action)?.Invoke();
    } */
    /* int[] v = new int[] { 11, 5, 90 };
    FunctionEntera f = n => n * 2;
    Aplicar(v, f);
    Imprimir(v);
    Aplicar(v, n => n + 10);
    Imprimir(v); */
  }

  void Metodo1()
    => Console.WriteLine("Ejecutando Metodo1");
  void Metodo2()
    => Console.WriteLine("Ejecutando Metodo2");
  int SumaUno(int n) => n + 1;
  int SumaDos(int n) => n + 2;

  void Aplicar(int[] v, FunctionEntera f)
  {
    for (int i = 0; i < v.Length; i++)
    {
      v[i] = f(v[i]);
    }
  }

  void Imprimir(int[] v)
  {
    /* El for each no puede modificar el valor en tiempo de ejecución */
    foreach (int i in v)
    {
      Console.Write(i + " ");
    }
    Console.WriteLine();
  }
}