namespace Teoria8;

class Trabajador
{
  public Action? TrabajoFinalizado;
  public void Trabajar()
  {
    Console.WriteLine("trabajador trabajando...");
    if (TrabajoFinalizado != null) { TrabajoFinalizado(); }
  }
}