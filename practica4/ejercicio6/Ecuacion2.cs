namespace ejercicio6;

class Ecuacion2
{
  private double _a;
  private double _b;
  private double _c;

  public Ecuacion2(double a, double b, double c)
  {
    _a = a;
    _b = b;
    _c = c;
  }

  /*  Devuelve el valor del discriminante (double), el discriminante tiene la siguiente formula, (b^2)-4*a*c. */
  public double GetDiscriminante() => (Math.Pow(_b, 2) - (4 * _a * _c));

  /*  Devuelve 0, 1 ó 2 dependiendo de la cantidad de raíces reales que posee la 
      ecuación. Si el discriminante es negativo no tiene raíces reales, si el discriminante es cero tiene una
      única raíz, si el discriminante es mayor que cero posee 2 raíces reales. */
  public int GetCantidadDeRaices() => (GetDiscriminante() < 0.0) ? 0 : (GetDiscriminante() == 0.0) ? 1 : 2;

  /*  Imprime la única o las 2 posibles raíces reales de la ecuación. En caso de no poseer
      soluciones reales debe imprimir una leyenda que así lo especifique. */
  public string? ImprimirRaices()
  {
    List<double> raices = new List<double>();

    CalcularDivisores(_c);

    foreach (int item in raices)
    {
      if (((Math.Pow(item, 2) * _a + item * _b + _c) != 0) || ((Math.Pow(item, 2) * _a + (item * -1) * _b + _c) != 0)) raices.Remove(item);
    }
                                                                                                                                                
    void CalcularDivisores(double val)
    {
      for (int i = 1; i <= val; i++)
      {
        if ((val % i) == 0)
        {
          raices.Add(i);
        }
      }
    }
    return raices.Count == 0 ? "Este polinomio no tiene raices" : raices.ToString();
  }
}