namespace ejercicio5;


class Hora
{
  private int _hora, _min, _seg;
  private bool _dou;
  public Hora(int hora, int min, int seg)
  {
    this._hora = hora;
    this._min = min;
    this._seg = seg;
    _dou = false;
  }

  public Hora(double hour)
  {
    _hora = (int) (hour);
    _min = (int) ((60 * (hour % 1)));
    _seg = (int) ((60 * (((hour % 1) * 60) % 1)) * 1000);
    _dou = true;
  }

  public void Imprimir() => Console.WriteLine($"{_hora} horas, {_min} minutos, {(_dou.Equals(true) ? Convert.ToDouble(_seg / 1000).ToString("F3") : _seg)} segundos");
}