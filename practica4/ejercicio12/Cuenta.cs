namespace ejercicio12;

class Cuenta
{
  private double _monto;
  private int _titularDNI;
  private string? _titularNombre;

  public Cuenta(int i) : this(null, i) { }

  public Cuenta(string? nombre) : this(nombre, 0) { }

  public Cuenta(string? nombre, int i)
  {
    _titularDNI = i;
    _titularNombre = nombre;
    _monto = 0;
  }

  public Cuenta() : this(null, 0) { }
  public void Imprimir()
  {
    Console.Write($"{(_titularNombre == null ? "No especificado" : _titularNombre)}, ");
    Console.Write($"{(_titularDNI == 0 ? "No especificado" : _titularDNI)}, ");
    Console.WriteLine($"Monto: {_monto}");
  }

  public void Depositar(int i) => _monto += i;

  public void Extraer(int i)
  {
    if (_monto - i < 0)
    {
      Console.WriteLine("Operación cancelada, monto insuficiente");
    }
    else
    {
      _monto -= i;
    }
  }
}