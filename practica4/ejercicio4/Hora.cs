namespace ejercicio4;


class Hora
{
  private int _hora, _min, _seg;

  public Hora(int hora, int min, int seg)
  {
    this._hora = hora;
    this._min = min;
    this._seg = seg;
  }

  public void Imprimir() => Console.WriteLine($"{_hora} horas, {_min} minutos, {_seg} segundos");
}
