﻿// See https://aka.ms/new-console-template for more information
object o = 5;
Sobrecarga s = new Sobrecarga();
s.Procesar(o, o); /* 5 object */ /* entero: 5 objeto: 5 */
s.Procesar((dynamic)o, o); /* 5 5 */ /* entero: 5 objeto: 5 */
s.Procesar((dynamic)o, (dynamic)o); /* 5 5 */ /* dynamic d1: 5 dynamic d2:5 */
s.Procesar(o, (dynamic)o); /* 5 object */ /* entero: 5 objeto: 5 */
s.Procesar(5, 5); /* 5 5 */ /* ????? */

/* Tanto si se pasa el primero o el segundo parámetro como objeto va a ese método */
/* En cambio si se castean ambos como "algo" va a ese que tenga "algo" en ambos */

class Sobrecarga
{
  public void Procesar(int i, object o)
  {
    Console.WriteLine($"entero: {i} objeto:{o} ");
  }
  public void Procesar(dynamic d1, dynamic d2)
  {
    Console.WriteLine($"dynamic d1: {d1} dynamic d2:{d2} ");
  }
}
