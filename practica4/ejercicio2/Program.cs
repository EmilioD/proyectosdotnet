﻿// See https://aka.ms/new-console-template for more information
/* Modificar el programa anterior haciendo privados todos los campos. Definir un constructor adecuado y un
método público Imprimir() que escriba en la consola los campos del objeto con el formato requerido para el
listado. */

using ejercicio2;

List<Persona> lista = new List<Persona>();
string? name;
int age, dni;

Console.WriteLine("Ingreso de datos - finalize con el nombre Fin");
Console.Write("Nombre: "); name = Console.ReadLine();
if (!name.Equals("Fin"))
{
  Console.Write("Edad: "); age = int.Parse(Console.ReadLine());
  Console.Write("DNI: "); dni = int.Parse(Console.ReadLine());
  lista.Add(new Persona(name, age, dni));
}

while (!name.Equals("Fin"))
{
  Console.Write("Nombre: "); name = Console.ReadLine();
  if (!name.Equals("Fin"))
  {
    Console.Write("Edad: "); age = int.Parse(Console.ReadLine());
    Console.Write("DNI: "); dni = int.Parse(Console.ReadLine());
    lista.Add(new Persona(name, age, dni));
  }
}

int x = 1;
foreach (var item in lista)
{
  Console.WriteLine($"{x++}) {item.Imprimir()}");
}