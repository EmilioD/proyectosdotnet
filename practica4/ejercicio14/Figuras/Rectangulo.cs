namespace Figuras;

public class Rectangulo
{
  private double _a, _b;

  public Rectangulo(double a, double b)
  {
    _a = a;
    _b = b;
  }

  public Rectangulo(int a, int b) : this((double)a, (double)b) { }

  public string? GetArea() => $"{_a * _b}";
}