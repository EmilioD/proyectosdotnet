namespace Figuras;

public class Circulo
{
  private double _radio;

  public Circulo(double i) => _radio = i;

  public Circulo(int i) : this((double)i) { }

  public string? GetArea() => $"{Math.Pow(_radio, 2) * Math.PI}";
}