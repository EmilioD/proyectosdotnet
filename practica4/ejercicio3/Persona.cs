namespace ejercicio3;

class Persona
{
  private string? _nombre;
  private int _edad;
  private int _dni;

  public Persona(string? nombre, int edad, int dni)
  {
    this._nombre = nombre;
    this._edad = edad;
    this._dni = dni;
  }

  public Persona()
  {
    
  }

  public string Imprimir()
  {
    return $"{_nombre,10} {_edad,4} {_dni,16}";
  }


  public bool EsMayorQue(Persona p) => this._edad > p._edad;
}