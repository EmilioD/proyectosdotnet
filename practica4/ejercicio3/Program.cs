﻿// See https://aka.ms/new-console-template for more information
/* Agregar a la clase Persona un método EsMayorQue(Persona p) que devuelva verdadero si la persona que
recibe el mensaje tiene más edad que la persona enviada como parámetro. Utilizarlo para realizar un programa
que devuelva la persona más jóven de la lista. */
using ejercicio3;

List<Persona> lista = new List<Persona>();
string? name;
int age, dni;

Console.WriteLine("Ingreso de datos . . .");
Console.SetIn(new System.IO.StreamReader("/home/qudit/Documentos/proyectosDotNet/practica4/ejercicio3/People.txt"));
while ((name = Console.ReadLine()) != null)
{
  int.TryParse(Console.ReadLine(), out age);
  int.TryParse(Console.ReadLine(), out dni);
  lista.Add(new Persona(name, age, dni));
}
Console.WriteLine("Finalizado");

int min = int.MaxValue;
Persona min_per = new Persona();
min_per = lista.First();
foreach (var item in lista)
{
  if (min_per.EsMayorQue(item)) min_per = item;
}

Console.WriteLine($"La persona mas joven es: {min_per.Imprimir()}");