namespace ejercicio7;
using System.Collections;

class Nodo
{
  SortedSet<int> _arbol;

  public Nodo(int i)
  {
    _arbol = new SortedSet<int>();
    _arbol.Add(i);
  }

  public Nodo()
  {
    _arbol = new SortedSet<int>();
  }

  /* Inserta valor en el árbol descartándolo en caso que ya exista. */
  public void Insertar(int i) => _arbol.Add(i);

  /* Devuelve un ArrayList con los valores ordenados en forma creciente. */
  public ArrayList GetInOrder() => new ArrayList(_arbol.ToArray());

  /* Devuelve la altura del árbol. */
  public int GetAltura() => Convert.ToInt32(Math.Log2(_arbol.Count));

  /* Devuelve la cantidad de nodos que posee el árbol. */
  public int GetCantNodos() => _arbol.Count;

  /* Devuelve el valor máximo que contiene el árbol. */
  public int GetValorMaximo() => _arbol.Max;

  /* Devuelve el valor mínimo que contiene el árbol. */
  public int GetValorMinimo() => _arbol.Min;
}