﻿// See https://aka.ms/new-console-template for more information
/* Reemplazar estas líneas de código por otras equivalentes que utilicen el operador null-coalescing ( ?? ) y / o
la asignación null-coalescing (??=) */

string? st, st1, st2, st3, st4;
st1 = "uno";
st2 = null;
st3 = st2;
st4 = st3;

if (st1 == null)
{
  if (st2 == null)
  {
    st = st3;
  }
  else
  {
    st = st2;
  }
}
else
{
  st = st1;
}
if (st4 == null)
{
  st4 = "valor por defecto";
}

Console.WriteLine($"El valor es: {st = st1 ?? st2 ?? st3 ?? st4}");