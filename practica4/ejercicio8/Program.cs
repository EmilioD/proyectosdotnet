﻿// See https://aka.ms/new-console-template for more information
/* Implementar la clase Matriz que se utilizará para trabajar con matrices matemáticas. Implementar los dos
constructores y todos los métodos que se detallan a continuación:
Matriz(int filas, int columnas)
Matriz(double[,] matriz)
void SetElemento(int fila, int columna, double elemento)
double GetElemento(int fila, int columna)
void imprimir()
void imprimir(string formatString)
double[] GetFila(int fila)
double[] GetColumna(int columna)
double[] GetDiagonalPrincipal()
double[] GetDiagonalSecundaria()
double[][] getArregloDeArreglo()
void sumarle(Matriz m)
void restarle(Matriz m)
void multiplicarPor(Matriz m) */

using ejercicio8;

int F = 3;
int C = 3;
double[,] matriz = new double[F, C];
double[,] A = new double[F, C];
double[,] B = new double[F, C];
for (int i = 0; i < A.Length; i++)
{
  matriz[i / matriz.GetLength(0), i % matriz.GetLength(0)] = i;
  A[i / A.GetLength(0), i % A.GetLength(0)] = i;
  B[i / B.GetLength(0), i % B.GetLength(0)] = i;
}


Matriz matrix = new Matriz(matriz);
Matriz _A = new Matriz(A);
Matriz _B = new Matriz(B);

matrix.SetElemento(1, 1, 1);
matrix.GetElemento(1, 1);
matrix.imprimir();
matrix.imprimir("F3");
matrix.GetFila(1);
matrix.GetColumna(1);
matrix.GetDiagonalPrincipal();
matrix.GetDiagonalSecundaria();
matrix.getArregloDeArreglo();
matrix.sumarle(_A);
matrix.restarle(_B);
matrix.multiplicarPor(_A);
matrix.imprimir();
matrix.imprimir("F3");