namespace ejercicio8;

class Matriz
{

  double[,] _matriz;
  int _f, _c;

  public Matriz(int filas, int columnas)
  {
    _f = filas;
    _c = columnas;
    _matriz = new double[_f, _c];
  }
  public Matriz(double[,] matriz)
  {
    _f = matriz.GetLength(0);
    _c = matriz.GetLength(1);
    _matriz = new double[_f, _c];
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        _matriz[i, j] = matriz[i, j];
      }
    }
  }
  public void SetElemento(int fila, int columna, double elemento) => _matriz[fila, columna] = elemento;
  public double GetElemento(int fila, int columna) => _matriz[fila, columna];
  public void imprimir()
  {
    Console.WriteLine("Elementos de la matriz: ");
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        Console.Write($"| {_matriz[i, j],-3}");
      }
      Console.WriteLine(" | \n");
    }
  }
  public void imprimir(string formatString)
  {
    Console.WriteLine("Elementos de la matriz: ");
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        Console.Write($"| {_matriz[i, j],-3}");
      }
      Console.WriteLine(" | \n");
    }
  }
  public double[] GetFila(int fila)
  {
    double[] row = new double[_f];

    for (int i = 0; i < _c; i++)
    {
      row[i] = _matriz[fila, i];
    }
    return row;
  }
  public double[] GetColumna(int columna)
  {
    double[] col = new double[_c];

    for (int i = 0; i < _f; i++)
    {
      col[i] = _matriz[i, columna];
    }
    return col;
  }
  public double[] GetDiagonalPrincipal()
  {
    if (_f != _c) throw new ArgumentException("La matriz no es cuadrada");
    double[] diag = new double[_f];
    double[] diag_sec = new double[_f];
    for (int i = 0; i < _f; i++)
    {
      diag[i] = _matriz[i, i];
    }
    return diag;
  }
  public double[] GetDiagonalSecundaria()
  {
    if (_f != _c) throw new ArgumentException("La matriz no es cuadrada");
    double[] diag = new double[_f];
    double[] diag_sec = new double[_f];
    for (int i = 0; i < _f; i++)
    {
      diag[i] = _matriz[(_f - 1) - i, i];
    }
    return diag;
  }
  public double[][] getArregloDeArreglo()
  {
    double[][] result = new double[_f][];
    for (int i = 0; i < _f; i++)
    {
      result[i] = new double[_c];
    }

    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        result[i][j] = _matriz[i, j];
      }
    }
    return result;
  }
  public void sumarle(Matriz m)
  {
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        _matriz[i, j] += m.GetElemento(i, j);
      }
    }
  }
  public void restarle(Matriz m)
  {
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        _matriz[i, j] -= m.GetElemento(i, j);
      }
    }
  }
  public void multiplicarPor(Matriz m)
  {
    for (int i = 0; i < _f; i++)
    {
      for (int j = 0; j < _c; j++)
      {
        _matriz[i, j] *= m.GetElemento(j, i);
      }
    }
  }
}