﻿// See https://aka.ms/new-console-template for more information
/* Definir una clase Persona con 3 campos públicos: Nombre , Edad y DNI . Escribir un algoritmo que permita al
usuario ingresar en una consola una serie de datos de la forma Nombre,Documento,Edad<ENTER> . Una vez
finalizada la entrada de datos, el programa debe imprimir en la consola un listado con 4 columnas de la
siguiente forma:
Nro) Nombre Edad DNI.
Ejemplo de listado por consola:
1) Juan Perez 40 2098745
2) José García 41 1965412
...
NOTA: Puede utilizar: Console.SetIn(new System.IO.StreamReader(nombreDeArchivo));
para cambiar la entrada estándar de la consola y poder leer directamente desde un archivo de texto. */
using ejercicio1;

List<Persona> lista = new List<Persona>();
string? word;
Persona per;

Console.WriteLine("Ingreso de datos - finalize con el nombre Fin");
per = new Persona();
Console.Write("Nombre: "); per.Nombre = Console.ReadLine();
if (!per.Nombre.Equals("Fin"))
{
  Console.Write("Edad: "); per.Edad = int.Parse(Console.ReadLine());
  Console.Write("DNI: "); per.DNI = int.Parse(Console.ReadLine());
  lista.Add(per);
}

while (!per.Nombre.Equals("Fin"))
{
  per = new Persona();
  Console.Write("Nombre: "); per.Nombre = Console.ReadLine();
  if (!per.Nombre.Equals("Fin"))
  {
    Console.Write("Edad: "); per.Edad = int.Parse(Console.ReadLine());
    Console.Write("DNI: "); per.DNI = int.Parse(Console.ReadLine());
    lista.Add(per);
  }
}

int x = 1;
foreach (var item in lista)
{
  Console.WriteLine($"{x}) {item.Nombre, 10} {item.Edad, 4} {item.DNI, 16}");
}