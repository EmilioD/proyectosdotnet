﻿// See https://aka.ms/new-console-template for more information
/* ¿Qué se puede decir en relación a la sobrecarga de métodos en este ejemplo? */
/* Deberían funcionar ya que las diferencias son de los tipos de parámetro recibidos */
/* Y sobre el retorno del método */
class A
{
  public void Metodo(short n)
  {
    Console.Write("short {0} - ", n);
  }
  public void Metodo(int n)
  {
    Console.Write("int {0} - ", n);
  }
  public int Metodo(int n)
  {
    return n + 10;
  }
}

/* Bueno el último no fuciona ya que por mas que cambie el retorno no cambian los parámetros */