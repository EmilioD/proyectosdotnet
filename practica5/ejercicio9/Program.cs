﻿// See https://aka.ms/new-console-template for more information
Auto a = new Auto();
a.Marca = "Ford";
Console.WriteLine(a.Marca);
class Auto
{
  private string marca;
  public string Marca
  {
    set
    {
      Marca = value;
    }
    get
    {
      return marca;
    }
  }
}

/* El problema está en que la propiedad Marca, en el get, retorna el atributo privado marca, que nunca es asignado */
