﻿// See https://aka.ms/new-console-template for more information
/* Identificar todos los miembros en la siguiente declaración de clase. Indicar si se trata de un
constructor, método, campo, propiedad o indizador, si es estático o de instancia, y en caso que
corresponda si es de sólo lectura, sólo escritura o lectura y escritura. En el caso de las propiedades
indicar también si se trata de una propiedad auto-implementada.
Nota: La clase compila perfectamente. Sólo prestar atención a la sintaxis, la semántica es irrelevante. */
class A
{
  /* Campo estático */
  private static int a;

  /* Campo estático solo lectura*/
  private static readonly int b;

  /* Constructor protected de instancia */
  A() { }
  
  /* Constructor de instancia */
  public A(int i) : this() { }

  /* Constructor estático de clase */
  static A() => b = 2;

  /* Campo de instancia */
  int c;

  /* Método de clase estático */
  public static void A1() => a = 1;

  /* Método público de instancia */
  public int A1(int a) => A.a + a;

  /* Propiedad de instancia estática */
  public static int A2;
  
  /* Propiedad estática de solo lectura */
  static int A3 => 3;
  
  /* Propiedad privada de clase de solo lectura */
  private int A4() => 4;
  
  /* Propiedad pública de clase de solo lectura */
  public int A5 { get => 5; }

  /* Propiedad protected de clase de solo escritura*/
  int A6 { set => c = value; }

  /* Propiedad pública de clase de escritura/lectura */
  public int A7 { get; set; }

  /* Propiedad pública de clase de solo lectura auto-implementada*/
  public int A8 { get; } = 8;

  /* Indizador de clase solo lectura */
  public int this[int i] => i;
}
