namespace ejercicio3;

class Cuenta
{
  private static int s_total;
  private double _monto;
  private int _titularDNI;
  private string? _titularNombre;
  private static List<Cuenta> s_cuentas = new List<Cuenta>();
  public static int Total
  {
    get => s_total;
    set => s_total += value;
  }
  public double Monto
  {
    get => _monto;
    set => _monto += value;
  }
  public string Id { get; }
  public static int Cuentas { get; set; }
  public static int Depositos { get; set; }
  public static int TotalDepositos { get; set; }
  public static int Extracciones { get; set; }
  public static int TotalExtracciones { get; set; }
  public static int ExtraccionesFallidas { get; set; }
  public static List<Cuenta> GetCuentas => s_cuentas.ToList();

  public Cuenta(int i) : this(null, i) { }

  public Cuenta(string? nombre) : this(nombre, 0) { }

  public Cuenta(string? nombre, int i)
  {
    _titularDNI = i;
    _titularNombre = nombre;
    Monto = 0;
    Id = Guid.NewGuid().ToString();
    s_cuentas.Add(this);
    Console.WriteLine($"Se creó la cuenta Id={Id}");
  }
  public Cuenta() : this(null, 0) { }
  public void Imprimir()
  {
    Console.Write($"{(_titularNombre == null ? "No especificado" : _titularNombre)}, ");
    Console.Write($"{(_titularDNI == 0 ? "No especificado" : _titularDNI)}, ");
    Console.WriteLine($"Monto: {Monto}");
  }
  public static void GetResumen() => Console.WriteLine($"Total: {Total}");
  public Cuenta Depositar(int i)
  {
    Monto = i;
    Console.WriteLine($"Se depositó {i} en la cuenta {Id} (Saldo={Monto})");
    Cuenta.Depositos++;
    Cuenta.TotalDepositos += i;
    return this;
  }
  public Cuenta Extraer(int i)
  {
    if (Monto - i < 0)
    {
      Console.WriteLine("Operación denegada, monto insuficiente");
      Cuenta.ExtraccionesFallidas++;
    }
    else
    {
      Console.WriteLine($"Se extrajo {i} de la cuenta {Id} (Saldo={Monto})");
      Cuenta.Extracciones++;
      Cuenta.TotalExtracciones += i;
      Monto = i * (-1);
    }
    return this;
  }

  public Cuenta Depositar(double cantidad)
  {
    Monto += cantidad;
    Console.WriteLine($"Se depositó {cantidad} en la cuenta {Id} (Saldo={Monto})");
    Cuenta.Depositos++;
    Cuenta.TotalDepositos += (int)cantidad;
    return this;
  }

  public static void ImprimirDetalle()
  {
    Console.WriteLine($"CUENTAS CREADAS: {Cuenta.Cuentas,2}");
    Console.WriteLine($"DEPOSITOS: {Cuenta.Depositos,8} - Total depositado: {Cuenta.TotalDepositos,5}");
    Console.WriteLine($"EXTRACCIONES: {Cuenta.Extracciones,5} - Total extraido: {Cuenta.TotalExtracciones,7}");
    Console.WriteLine($"                    - Saldo: {Cuenta.TotalDepositos - Cuenta.TotalExtracciones,15}");
    Console.WriteLine($"\n* Se denegaron {Cuenta.ExtraccionesFallidas} extracciones por falta de fondos");
  }

  
}