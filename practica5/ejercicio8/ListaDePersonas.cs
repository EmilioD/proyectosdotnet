namespace ejercicio8;

class ListaDePersonas
{
  public List<Persona> Lista { get; set; }
  public ListaDePersonas()
  {
    Lista = new List<Persona>();
  }
  public ListaDePersonas(IEnumerable<Persona> array)
  {
    Lista = new List<Persona>();
    Lista.AddRange(array);
  }

  public Persona? this[int i]
  {
    get
    {
      foreach (Persona item in Lista)
      {
        if (item.DNI == i) return item;
      }
      return null;
    }
  }

  public List<string>? this[char c]
  {
    get
    {
      List<string> list = new List<string>();
      foreach (Persona item in Lista)
      {
        if (item.Nombre?[0] == c) list.Add(item.Nombre);
      }
      return list;
    }
  }
}