namespace ejercicio8;

class Persona
{
  public string? Nombre { get; set; }
  public char Sexo { get; set; }
  public int DNI { get; set; }

  public  DateTime FechaDeNacimiento { get; set; }

  public int Edad => int.Parse(DateTime.Now.Subtract(FechaDeNacimiento).ToString("YYYY"));

  public dynamic? this[int i]
  {
    get =>
      i == 0 ? Nombre : i == 1 ? Sexo : i == 2 ? DNI : i == 3 ? FechaDeNacimiento : i == 4 ? Edad : null;
    set
    {
      switch (i)
      {
        case 0:
          Nombre ??= value;
          break;
        case 1:
          Sexo = value;
          break;
        case 2:
          DNI = value;
          break;
        case 3:
          FechaDeNacimiento = value;
          break;
        case 4:
          break;
        default:
          break;
      }
    }
  }
}