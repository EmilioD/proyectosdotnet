﻿/* Modificar la definición de la clase Matriz realizada en la práctica 4. Eliminar los métodos
SetElemento (...) y GetElemento(...) . Definir un indizador adecuado para leer y escribir
elementos de la matriz. Eliminar los métodos GetDiagonalPrincipal() y GetDiagonalSecundaria()
reemplazándolos por las propiedades de sólo lectura DiagonalPrincipal y DiagonalSecundaria . */
using ejercicio5;

int F = 3;
int C = 3;
double?[,] matriz = new double?[F, C];
double?[,] A = new double?[F, C];
double?[,] B = new double?[F, C];
double?[] diag = new double?[F];
double?[] diag_sec = new double?[F];
for (int i = 0; i < A.Length; i++)
{
  matriz[i / matriz.GetLength(0), i % matriz.GetLength(0)] = i;
  A[i / A.GetLength(0), i % A.GetLength(0)] = i;
  B[i / B.GetLength(0), i % B.GetLength(0)] = i;
}


Matriz matrix = new Matriz(matriz);
Matriz _A = new Matriz(A);
Matriz _B = new Matriz(B);

matrix[1, 1] = 1;
Console.WriteLine($"{matrix[1, 1]}");
matrix.imprimir();
matrix.imprimir("F3");
matrix.GetFila(1);
matrix.GetColumna(1);
diag = matrix.DiagonalPrincipal;
diag_sec = matrix.DiagonalSecundaria;
matrix.getArregloDeArreglo();
matrix.sumarle(_A);
matrix.restarle(_B);
matrix.multiplicarPor(_A);
matrix.imprimir();
matrix.imprimir("F3");
