namespace CalculoSimple;
class ProveedorServicios
{
  public ILogger GetLogger()
  => new Logger();
  public ICalculador GetCalculator()
  => new Calculador(this.GetLogger());
}