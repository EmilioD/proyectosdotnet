namespace CalculoSimple;
class Calculador : ICalculador
{
  ILogger _logger = new Logger();
  public Calculador(ILogger logger)
  {
    _logger = logger;
  }
  public void Calcular(int n)
  {
    int resul = (n + 5) * (n + 7);
    _logger.Log($"Fin de Calculo - (resul = {resul})");
  }
}