﻿class Program
{
  static void Main(string[] args)
  {
    /* int a = 17;
    int b = 23;
    Swap<int>(ref a, ref b);
    Console.WriteLine($"a={a} y b={b}");
    string str1 = "hola";
    string str2 = "mundo";
    Swap<string>(ref str1, ref str2);
    Console.WriteLine($"str1={str1} y str2={str2}"); */

    int i = Maximo<int>(100, 55);
    Console.WriteLine(i);
    string st = (string)Maximo<string>("hola", "mundo");
    Console.WriteLine(st);
    Console.WriteLine(Maximo<char>('A','B'));
  }

  static void Swap<T>(ref T i, ref T j)
  {
    T auxi = i;
    i = j;
    j = auxi;
  }

  static T Maximo<T>(T a, T b) where T : IComparable
  {
    return a.CompareTo(b) > 0 ? a : b;

    /* if (a.CompareTo(b) > 0)
    {
      return a;
    }
    return b; */
  }
}
