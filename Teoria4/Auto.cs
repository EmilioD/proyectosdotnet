namespace Teoria4;

class Auto
{
  private string _marca;
  private int _modelo;

  public Auto(string marca, int modelo)
  {
    _marca = marca;
    _modelo = modelo;
  }

  public Auto()
  {
    _marca = "Fiat";
    _modelo = DateTime.Now.Year;
  }

  public Auto(string marca) : this()
  {
    _marca = marca;
  }

  /* Otra forma (no hacer) */
  /* public Auto(string marca) : this(marca, DateTime.Now.Year){} */

  public string GetDescripcion() => $"Auto {_marca} {_modelo}";

  public void setMarca(string marca) => _marca = marca;

  public void setModelo(int modelo) => _modelo = modelo;
}