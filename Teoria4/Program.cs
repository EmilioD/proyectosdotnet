﻿// See https://aka.ms/new-console-template for more information
using Teoria4;

Auto a;
a = new Auto("Nissan", 2017);
Console.WriteLine(a.GetDescripcion());
Auto b = new Auto("Ford", 2015);
Console.WriteLine(b.GetDescripcion());
a = new Auto("Renault");
Console.WriteLine(a.GetDescripcion());