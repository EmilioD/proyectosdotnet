namespace Teoria5;

class Cuadrado
{
  public double Lado { get; set; }
  public double Area => Lado * Lado;

}