namespace Teoria5;

class Cuenta
{
  private static int s_total;
  private static int Total
  {
    get => s_total;
    set => s_total += value;
  }
  public double Monto { get; set; }
  public static void GetResumen() => Console.WriteLine($"Total: {Total}");
  public Cuenta Depositar(double cantidad)
  {
    Monto += cantidad;
    return this;
  }
}