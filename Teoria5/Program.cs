﻿// See https://aka.ms/new-console-template for more information
using Teoria5;

/* Cuenta c1 = new Cuenta();
c1.Monto = 20;
Cuenta.Total += c1.Monto;
Cuenta c2 = new Cuenta();
c2.Monto = 20;
Cuenta.Total += c2.Monto;
Cuenta.GetResumen(); */

/* Cuadrado c = new Cuadrado();
c.Lado = 2.5;
Console.WriteLine($"Lado: {c.Lado}, área: {c.Area}"); */

Familia f = new Familia();
f.Padre = new Persona("Juan", 50);
f[1] = new Persona("María", 40);
f[2] = new Persona("José", 15);
for (int i = 0; i < 3; i++)
{
  f[i]?.Imprimir();
}
