﻿// See https://aka.ms/new-console-template for more information
/* 
var x = new { Nombre = "Juan", Edad = 29 };
Console.WriteLine(x.Nombre.GetType());
*/

/* string marca = "Ford";
int modelo = 2000;
string st;
Console.WriteLine("Es un " + marca + " año " + modelo); */

/* string marca = "Ford";
int modelo = 2000;
string st;
st = string.Format("Es un {0} año {1}", marca, modelo);
Console.WriteLine(st); */

/* string marca = "Ford";
int modelo = 2000;
string st = $"Es un {marca} año {modelo}";
Console.WriteLine(st); */

/* string marca = "Ford";
int modelo = 2000;
Console.WriteLine($"Es un {marca,-7} año {modelo}");
Console.WriteLine($"Es un {"Nissan",-7} año {2020}"); */

/* double r = 2.417;
Console.WriteLine($"Valor = {r:0}");
Console.WriteLine($"Valor = {r:0.0}");
Console.WriteLine($"Valor = {r:0.00}");
Console.WriteLine($"Valor = {r:0.000}");
Console.WriteLine($"Valor = {r:0.0000}"); */

/* List<int> lista = new List<int>() { 10, 20, 30, 40 };
int[] vector = new int[] { 31, 32, 33 };
lista.InsertRange(3, vector);
lista.Insert(2,22);
for (int i = 0; i < lista.Count; i++)
{
  Console.WriteLine(lista[i]);
} */

/* void Procesar(double[]? v, int i, int c){
  try
  {  
    c += 100;
    v[i] = 1000 / c;
    Console.WriteLine(v[i]);
  }
  catch (Exception)
  {
    Console.WriteLine("No procesado");
  }
} */

void Procesar(double[]? v, int i, int c){
  try
  {  
    c += 100;
    v[i] = 1000 / c;
    Console.WriteLine(v[i]);
  }
  catch (Exception e)
  {
    Console.WriteLine(e.Message);
  }
}
