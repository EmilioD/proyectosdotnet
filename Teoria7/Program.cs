﻿// See https://aka.ms/new-console-template for more information
using Teoria7;

/* IImprimible[] vector = new IImprimible[] {
       new Moto("Zanella"),
       new Empleado("Juan"),
       new Moto("Gilera")
   };

foreach (IImprimible o in vector)
{
  o.Imprimir();
}

 */

/* var vector = new Empleado[]{
   new Empleado("Juan"),
   new Empleado("Adriana"),
   new Empleado("Diego")
 };

Array.Sort(vector);

foreach (Empleado item in vector)
{
  item.Imprimir();
} */

Pyme miPyme = new Pyme(
  new Empleado("Juan") { Legajo = 79 },
  new Empleado("Adriana") { Legajo = 123 },
  new Empleado("Diego") { Legajo = 12 });

foreach (Empleado e in miPyme)
{
  e.Imprimir();
}