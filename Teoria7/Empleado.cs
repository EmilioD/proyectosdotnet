namespace Teoria7;

class Empleado : Persona, IImprimible, IComparable
{
  public Empleado(string nombre)
     => Nombre = nombre;
  public int Legajo { get; set; }
  public void Imprimir(){
    Console.Write($"Soy el empleado {Nombre}");
    Console.WriteLine($", Legajo: {Legajo}" );
  }

  public int CompareTo(object? p)
  {
    int result = 0;
    if (p is Empleado) result = this.Nombre.CompareTo(((Empleado)p).Nombre);
    return result;
  }
}