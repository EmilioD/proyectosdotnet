#!/bin/zsh

DIR="practica$1"
mkdir -p ${DIR}
cd ${DIR}

echo ""
echo "Directorio creado..."
echo ""

for ((i=1;i<=$2;i++))
do
  echo ""
  echo "Creando proyecto .NET [ejercicio${i}]"
  dotnet new console -o "ejercicio${i}"
  echo ""
done
