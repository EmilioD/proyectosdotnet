﻿// See https://aka.ms/new-console-template for more information
/* 1. Ejecutar y analizar cuidadosamente el siguiente programa: */

Console.CursorVisible = false;
ConsoleKeyInfo k = Console.ReadKey(true);
while (k.Key != ConsoleKey.End)
{
  Console.Clear();
  Console.Write($"{k.Modifiers}-{k.Key}-{k.KeyChar}");
  k = Console.ReadKey(true);
}

/* 
Obtains the next character or function key pressed by the user. The pressed key is optionally displayed in the console window.
Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.
An object that describes the ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The ConsoleKeyInfo object also describes, in a bitwise combination of ConsoleModifiers values, whether one or more Shift, Alt, or Ctrl modifier keys was pressed simultaneously with the console key.
 */