﻿/* Implementar un método que devuelva un arreglo de arreglos con los mismos elementos que la
matriz pasada como parámetro: */


int F = 3;
int C = 3;
double[,] matriz = new double[F, C];

for (int i = 0; i < matriz.Length; i++)
{
  matriz[i / F, i % C] = i;
}


GetArregloDeArreglo(matriz);

double[][] GetArregloDeArreglo(double[,] matriz)
{
  double[][] result = new double[F][];
  for (int i = 0; i < F; i++)
  {
    result[i] = new double[C];
  }

  for (int i = 0; i < F; i++)
  {
    for (int j = 0; j < C; j++)
    {
      result[i][j] = matriz[i, j];
    }
  }
  Console.Write("Elementos de la matriz generada: ");
  for (int i = 0; i < F; i++)
  {
    for (int j = 0; j < C; j++)
    {
      Console.Write("{0} ", result[i][j]);
    }
  }
  return result;
}
