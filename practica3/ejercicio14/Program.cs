﻿// See https://aka.ms/new-console-template for more information
/* Utilizar la clase Stack<T> (pila) para implementar un programa que pase un número en base
10 a otra base realizando divisiones sucesivas. Por ejemplo para pasar 35 en base 10 a binario
dividimos sucesivamente por dos hasta encontrar un cociente menor que el divisor, luego el
resultado se obtiene leyendo de abajo hacia arriba el cociente de la última división seguida por
todos los restos. */

Stack<int> pila = new Stack<int>();

int PasajeRecursivo(Stack<int> pila, int num)
{
  if (num >= 2)
  {
    pila.Push(PasajeRecursivo(pila, (int)num / 2));
  }
  return num % 2;
}

pila.Push(PasajeRecursivo(pila, 35));

Console.Write("35 convertido a binario: ");
foreach (var item in pila)
{
  Console.Write($"{item}");
}