﻿// See https://aka.ms/new-console-template for more information
/* Utilizar la clase Queue<T> para implementar un programa que realice el cifrado de un texto
aplicando la técnica de clave repetitiva. La técnica de clave repetitiva consiste en desplazar un
carácter una cantidad constante de acuerdo a la lista de valores que se encuentra en la clave. Por
ejemplo: para la siguiente tabla de caracteres:
A B C D E F G H I  J  K L   M N   Ñ O   P Q   R S   T U  V  W  X  Y  Z  sp
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 
Si la clave es {5,3,9,7} y el mensaje a cifrar “HOLA MUNDO”
Se cifra de la siguiente manera:
H  O   L   A  sp M  U  N  D O ← Mensaje original
8  16  12  1  28 13 22 14 4 16 ← Código sin cifrar
5  3   9   7  5  3  9 7  5  3← Clave repetida
13 19  21  8  5 16 3 21 9 19 ← Código cifrado
M  R   T   H  E O  C T  I R ← Mensaje cifrado
A cada carácter del mensaje original se le suma la cantidad indicada en la clave. En el caso que
la suma fuese mayor que 28 se debe volver a contar desde el principio, Implementar una cola
con los números de la clave encolados y a medida que se desencolen para utilizarlos en el
cifrado, se vuelvan a encolar para su posterior utilización. Programar un método para cifrar y
otro para descifrar.*/

using System.Text;

/* Uso StringBuilder para acceder a cada índice del string y modificar char a char*/
StringBuilder st = new StringBuilder("HOLA MUNDO");
/* Para guardar cada char con su int */
Dictionary<char, int> word = new Dictionary<char, int>();
/* Clave modificadora */
Queue<int> q = new Queue<int>();

/* Cargo en el hash las letras en el rango A-Z con su número */
void InitializeHash(Dictionary<char, int> word)
{
  int x = 1;
  for (int i = 65; i < 91; i++)
  {
    if (i == 78)
    {
      word.Add((char)i, x++);
      word.Add('Ñ', 15);
      x++;
    }
    else
    {
      word.Add((char)i, x++);
    }
  }
  word.Add(' ', 28);
  /* foreach (var item in word)
  {
    Console.WriteLine($"Clave: {item.Key} | Valor: {item.Value}");
  } */
}

/* Tengo que tomar el elemento del string, pasarlo a int, desencolar un elemento(guardar en var), sumarlo al int ant */
/* guardar esa suma en el string original, y encolar el mismo elemento guardado */
void Cifrar(StringBuilder st, Queue<int> q, Dictionary<char, int> word)
{
  Console.WriteLine($"String original: {st.ToString()}");
  int temp;
  for (int i = 0; i < st.Length; i++)
  {
    temp = q.Dequeue();
    Console.WriteLine($"Elemento desencolado: {temp}");
    Console.WriteLine($"Char del string: {st[i]}");
    Console.WriteLine($"Clave en el hash: {word[st[i]]}");
    Console.WriteLine($"Int a convertir en char: {word[st[i]] - temp}");
    Console.WriteLine((word[st[i]] + temp > 28) ? $"{ReturnKeyForValue(word, (word[st[i]] + temp) - 28)}" : $"{ReturnKeyForValue(word, word[st[i]] + temp)}");
    /* Si la suma > 28, entonces comienzo desde 1 + la diferencia, sino asigno normal */
    st[i] = (word[st[i]] + temp > 28) ? ReturnKeyForValue(word, (word[st[i]] + temp) - 28) : ReturnKeyForValue(word, word[st[i]] + temp);
    q.Enqueue(temp);
  }
  Console.WriteLine($"String cifrado: {st.ToString()}");
}

/* Tengo que tomar el elemento del string, pasarlo a int, desencolar un elemento(guardar en var), restarle al int ant */
/* guardar esa resta en el string original, y encolar el mismo elemento guardado  */

void Descifrar(StringBuilder st, Queue<int> q, Dictionary<char, int> word)
{
  Console.WriteLine($"String original: {st}");
  int temp;
  for (int i = 0; i < st.Length; i++)
  {
    temp = q.Dequeue();
    Console.WriteLine($"Elemento desencolado: {temp}");
    Console.WriteLine($"Char del string: {st[i]}");
    Console.WriteLine($"Clave en el hash: {word[st[i]]}");
    Console.WriteLine($"Int a convertir en char: {word[st[i]] - temp}");
    Console.WriteLine((word[st[i]] - temp < 1) ? $"{ReturnKeyForValue(word, 28 + (word[st[i]] - temp))}" : $"{ReturnKeyForValue(word, word[st[i]] - temp)}");
    /* Si la resta es menor a 1, entonces comienzo desde 28 - la diferencia */
    st[i] = (word[st[i]] - temp < 1) ? ReturnKeyForValue(word, 28 + (word[st[i]] - temp)) : ReturnKeyForValue(word, word[st[i]] - temp);
    q.Enqueue(temp);
  }
  Console.WriteLine($"String cifrado: {st}");
}

/* Retorna la clave para un cierto valor, en caso de no encontrar retorna null */
dynamic ReturnKeyForValue(Dictionary<char, int> dict, int value)
{
  foreach (KeyValuePair<char, int> pair in dict)
  {
    if (EqualityComparer<int>.Default.Equals(pair.Value, value))
    {
      return pair.Key;
    }
  }
  return default;
}

void InitializeQ(Queue<int> q)
{
  if (q.Count != 0) q.Clear();
  q.Enqueue(5);
  q.Enqueue(3);
  q.Enqueue(9);
  q.Enqueue(7);
}

InitializeQ(q);
InitializeHash(word);
Cifrar(st, q, word);
InitializeQ(q);
Descifrar(st, q, word);