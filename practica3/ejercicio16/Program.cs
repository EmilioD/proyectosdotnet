﻿// See https://aka.ms/new-console-template for more information
/* Implementar un programa que permita al usuario ingresar números por la consola. Debe
ingresarse un número por línea finalizado el proceso cuando el usuario ingresa una línea vacía.
A medida que se van ingresando los valores el sistema debe mostrar por la consola la suma
acumulada de los mismos. Utilizar la instrucción try/catch para validar que la entrada
ingresada sea un número válido, en caso contrario advertir con un mensaje al usuario y
proseguir con el ingreso de datos. */

int suma = 0;
String? word = Console.ReadLine();

if (!word.Equals(null))
  while (!word.Equals(" "))
  {
    try
    {
      int x = int.Parse(word);
      suma += x;
      Console.WriteLine($"{suma}");
    }
    catch (System.Exception e)
    {
      Console.WriteLine(e.Message);
    }
    word = Console.ReadLine();
  }