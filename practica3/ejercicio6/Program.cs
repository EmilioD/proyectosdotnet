﻿// See https://aka.ms/new-console-template for more information
/* Implementar los siguientes métodos que devuelvan la suma, resta y multiplicación de matrices
pasadas como parámetros. Para el caso de la suma y la resta, las matrices deben ser del mismo
tamaño, en caso de no serlo devolver null. Para el caso de la multiplicación la cantidad de
columnas de A debe ser igual a la cantidad de filas de B, en caso contrario generar una
excepción ArgumentException. */

/* 
double[,]? Suma(double[,] A, double[,] B)
double[,]? Resta(double[,] A, double[,] B)
double[,] Multiplicacion(double[,] A, double[,] B)
*/

int F = 3;
int C = 3;
double[,] matrix = new double[F, C];
double[,] A = new double[F, C];
double[,] B = new double[F, C];
for (int i = 0; i < A.Length; i++)
{
  A[i / A.GetLength(0), i % A.GetLength(0)] = i;
  B[i / B.GetLength(0), i % B.GetLength(0)] = i;
}

Console.WriteLine("Matriz A(son iguales): ");
imprimirMatriz(A);
Console.WriteLine("Suma");
matrix = Suma(A, B);
if (matrix != null ) imprimirMatriz(matrix);
Console.WriteLine("Resta");
matrix = Resta(A, B);
if (matrix != null ) imprimirMatriz(matrix);
Console.WriteLine("Multiplicación");
matrix = Multiplicacion(A, B);
if (matrix != null ) imprimirMatriz(matrix);


double[,]? Suma(double[,] A, double[,] B)
{
  if (A.Length != B.Length) return null;
  int rows = A.GetLength(0);
  int cols = A.GetLength(1);
  double[,]? result = new double[rows, cols];
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      result[i, j] = A[i, j] + B[i, j];
    }
  }

  return result;
}

double[,]? Resta(double[,] A, double[,] B)
{
  if (A.Length != B.Length) return null;
  int rows = A.GetLength(0);
  int cols = A.GetLength(1);
  double[,]? result = new double[rows, cols];
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      result[i, j] = A[i, j] - B[i, j];
    }
  }
  return result;
}

double[,] Multiplicacion(double[,] A, double[,] B)
{
  int rows = A.GetLength(0);
  int cols = A.GetLength(1);
  if (A.GetLength(1) != B.GetLength(0)) throw new ArgumentException("La dimensión de las columnas de A no es igual a las filas de B");
  double[,]? result = new double[rows, cols];
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      result[i, j] = A[i, j] * B[j, i];
    }
  }
  return result;
}

void imprimirMatriz(double[,]? matriz){
  for (int i = 0; i < matriz.GetLength(0); i++)
  {
    for (int j = 0; j < matriz.GetLength(1); j++)
    {
      Console.Write($"| {matriz[i, j], -3}");
    }
    Console.WriteLine("| \n");
  }
}