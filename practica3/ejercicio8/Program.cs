﻿// See https://aka.ms/new-console-template for more information
/* ¿Qué líneas del siguiente método provocan error de compilación y por qué? */
var a = 3L; //signed integer long
dynamic b = 32;
object obj = 3;
a = a * 2.0; //da error porque es un entero y no se está casteando, la multiplicación retorna un doubleqq
b = b * 2.0;
b = "hola";
obj = b;
b = b + 11;
obj = obj + 11; //no se pueden hacer operaciones aritméticas con un objeto, por mas que se le asigne un int
var c = new { Nombre= "Juan" };
var d = new { Nombre = "María" };
var e = new { Nombre = "Maria", Edad = 20 };
var f = new { Edad = 20, Nombre = "Maria" };
f.Edad = 22; // no se puede asignar valores a los atributos de un tipo anónimo
d = c;
e = d; // el tipo anónimo no es el mismo ya que no tienen los mismos atributos
f = e; // no tienen referencia al mismo tipo de objeto instanciado
// por mas que sean los mismos atributos, no están en el mismo orden
