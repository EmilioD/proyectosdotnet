﻿// See https://aka.ms/new-console-template for more information
/* Señalar errores de compilación y/o ejecución en el siguiente código */
object obj = new int[10];
dynamic dyn = 13;
/* No puedo pedir el length de un tipo objeto por mas que se cree una referencia a un arreglo */
/* No pasa por el compilador */
Console.WriteLine(obj.Length); 
/* En este caso como es dynamic pasa por el compilador, pero cuando se ejecute dará error */
/* Ya que se estará pidiendo el Length de un tipo Int */
Console.WriteLine(dyn.Length);
