﻿// See https://aka.ms/new-console-template for more information
/* Implementar los métodos GetDiagonalPrincipal y GetDiagonalSecundaria que
devuelven un vector con la diagonal correspondiente de la matriz pasada como parámetro. Si la
matriz no es cuadrada generar una excepción ArgumentException. */

int F = 3;
int C = 3;
double[,] matriz = new double[F, C];

for (int i = 0; i < F*F; i++)
{
  matriz[i / 3, i % 3] = i;
}

double[] GetDiagonalPrincipal(double[,] matriz)
{
  if (F != C) throw new ArgumentException("La matriz no es cuadrada");
  double[] diag = new double[F];
  double[] diag_sec = new double[F];
  for (int i = 0; i < F; i++)
  {
    diag[i] = matriz[i, i];
  }
  Console.Write("Diagonal principal: ");
  foreach (var item in diag)
  {
    Console.Write("{0} ", item);
  }
  return diag;
}

double[] GetDiagonalSecundaria(double[,] matriz)
{
  if (F != C) throw new ArgumentException("La matriz no es cuadrada");
  double[] diag = new double[F];
  double[] diag_sec = new double[F];
  for (int i = 0; i < F; i++)
  {
    diag[i] = matriz[(F - 1) - i, i];
  }
  Console.Write("Diagonal secundaria: ");
  foreach (var item in diag)
  {
    Console.Write("{0} ", item);
  }
  return diag;
}

try
{
  GetDiagonalPrincipal(matriz);
  GetDiagonalSecundaria(matriz);
}
catch (ArgumentException e)
{
  Console.WriteLine(e.Message);
}