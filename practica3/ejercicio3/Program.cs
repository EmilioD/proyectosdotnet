﻿// See https://aka.ms/new-console-template for more information
/* Implementar el método ImprimirMatrizConFormato, similar al anterior pero ahora con un
parámetro más que representa la plantilla de formato que debe aplicarse a los números al
imprimirse. La plantilla de formato es un string de acuerdo a las convenciones de formato
compuesto, por ejemplo “0.0” implica escribir los valores reales con un dígito para la parte
decimal. */

int F = 3;
int C = 3;
double[,] matriz = new double[F, C];

for (int i = 0; i < 9; i++)
{
  matriz[i / C, i % C] = i * Math.PI;
}

ImprimirMatrizConFormato(matriz, "0.0");

void ImprimirMatrizConFormato(double[,] matriz, string formatString)
{
  for (int i = 0; i < F; i++)
  {
    for (int j = 0; j < C; j++)
    {
      Console.Write($"{matriz[i, j].ToString(formatString), 8} ");
    }
    Console.WriteLine(" -- ");
  }
}

