﻿// See https://aka.ms/new-console-template for more information
/* ¿De qué tipo quedan definidas las variables x, y, z en el siguiente código? */
var i = 10; //int
var x = i * 1.0; //double
var y = 1f; //single
var z = i * y; //single
Console.WriteLine(i.GetType());
Console.WriteLine(x.GetType());
Console.WriteLine(y.GetType());
Console.WriteLine(z.GetType());
