﻿// See https://aka.ms/new-console-template for more information
/* Implementar un programa calculadora que calcule una expresión ingresada por el usuario
correspondiente a una operación binaria de las cuatro elementales (ejemplo “44.5/456”,
“456*45”, “549-12”, “54+6” ). Utilizar try/catch para validar los números y controlar
cualquier tipo de excepción que pudiese ocurrir en la evaluación de la expresión. */

double a, b, op = 0.0;
String? word;

Console.Write("Ingrese el primer operando: ");
InputOperand(out a);
Console.Write("Ingrese el segundo operando: ");
InputOperand(out b);
Console.Write("Ingrese el operador: ");
word = Console.ReadLine();

switch (word)
{
  case "+":
    op = a + b;
    break;
  case "-":
    op = a - b;
    break;
  case "*":
    op = a * b;
    break;
  case "/":
    try
    {
      op = a / b;
    }
    catch (System.Exception e)
    {
      Console.WriteLine(e.Message);
    }
    break;
  default: break;
}

Console.WriteLine($"Resultado: {(double) op}");

void InputOperand(out double x)
{
  try
  {
    x = double.Parse(Console.ReadLine());
  }
  catch (System.Exception e)
  {
    Console.WriteLine(e.Message);
    Console.Write("Intente de nuevo: ");
    InputOperand(out x);
  }
}