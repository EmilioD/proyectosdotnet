﻿// See https://aka.ms/new-console-template for more information
/* 15. ¿Qué salida por la consola produce el siguiente código?
¿Qué se puede inferir respecto de la excepción división por cero en relación al tipo de los
operandos? */

int x = 0;
try
{
  Console.WriteLine(1.0 / x);
  Console.WriteLine(1 / x);
  Console.WriteLine("todo OK");
}
catch (Exception e)
{
  Console.WriteLine(e.Message);
}

/* Imprime el mensaje de la excepción de división por 0 */
/* En la división por 0 de tipo flotante el resultado es infinito... ? */
/* En cambio en división de decimales recibimos la excepción */