﻿// See https://aka.ms/new-console-template for more information
/* Realizar un análisis sintáctico para determinar si los paréntesis en una expresión aritmética están
bien balanceados. Verificar que por cada paréntesis de apertura exista uno de cierre en la cadena
de entrada. Utilizar una pila para resolverlo. Los paréntesis de apertura de la entrada se
almacenan en una pila hasta encontrar uno de cierre, realizándose entonces la extracción del
último paréntesis de apertura almacenado. Si durante el proceso se lee un paréntesis de cierre y
la pila está vacía, entonces la cadena es incorrecta. Al finalizar el análisis, la pila debe quedar
vacía para que la cadena leída sea aceptada, de lo contrario la misma no es válida. */

String? st = "()()())";
Queue<char> q = new Queue<char>();

if (st != null)
  foreach (var item in st)
  {
    if (item.Equals('(')) q.Enqueue(item);

    if (item.Equals(')'))
    {
      if (q.Count == 0)
      {
        Console.WriteLine("Cadena no válida");
        break;
      }
      else
      {
        q.Dequeue();
      }
    }
  }

Console.WriteLine(q.Count != 0 ? "Cadena aceptada" : "Cadena no aceptada");
