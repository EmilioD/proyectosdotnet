﻿// See https://aka.ms/new-console-template for more information
/* Señalar errores de ejecución en el siguiente código */
List<int> a = new List<int>() { 1, 2, 3, 4 };
a.Remove(5); /* Va a buscar para eliminar el elemento 5 y no lo va a encontrar */
a.RemoveAt(4); /* Va a buscar para eliminar el elemento en el índice 4, pero la lista no se extiende hasta ese índice */
