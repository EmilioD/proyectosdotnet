﻿// See https://aka.ms/new-console-template for more information
/* Implementar un método para imprimir por consola todos los elementos de una matriz (arreglo
de dos dimensiones) pasada como parámetro. Debe imprimir todos los elementos de una fila en
la misma línea en la consola. */

int F = 3;
int C = 3;
double[,] matriz = new double[F, C];

for (int i = 0; i < 9; i++)
{
  matriz[i / C, i % C] = i;
}

ImprimirMatriz(matriz);

void ImprimirMatriz(double[,] matriz)
{
  for (int i = 0; i < F; i++)
  {
    for (int j = 0; j < C; j++)
    {
      Console.Write("{0} ", matriz[i, j]);
    }
    Console.WriteLine(" -- ");
  }
}