namespace ejercicio2;

class Persona : IAtendible
{
  public void SeAtiende() => Console.WriteLine("Atendiendo persona");
}