namespace ejercicio2;

class Perro : IVendible, IAtendible, ILavable
{
  public void SeAtiende() => Console.WriteLine("Atendiendo perro");
  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo perro a persona");

  public void SeLava() => Console.WriteLine("Lavando un perro");

  public void SeSeca() => Console.WriteLine("Secando perro");
}