namespace ejercicio2;

class Pelicula : IAlquilable
{
  public virtual void SeAlquilaA(Persona p)  => Console.WriteLine("Alquilando pelicula a persona");
  public virtual void SeDevuelveDe(Persona p)  => Console.WriteLine("Pelicula devuelta por persona");
}