namespace ejercicio2;

interface ILavable
{
  public void SeLava();
  public void SeSeca();
}