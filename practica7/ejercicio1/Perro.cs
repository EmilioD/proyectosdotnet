namespace ejercicio1;

class Perro : IVendible, IAtendible
{
  public void SeAtiende() => Console.WriteLine("Atendiendo perro");
  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo perro a persona");
}