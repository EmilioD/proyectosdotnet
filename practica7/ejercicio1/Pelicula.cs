namespace ejercicio1;

class Pelicula : IAlquilable
{
  public void SeAlquilaA(Persona p)  => Console.WriteLine("Alquilando pelicula a persona");
  public void SeDevuelveDe(Persona p)  => Console.WriteLine("Pelicula devuelta por persona");
}