namespace ejercicio1;

class Auto : IVendible, ILavable, IReciclable
{
  public void SeLava() => Console.WriteLine("Lavando auto");

  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo auto a persona");

  public void SeSeca() => Console.WriteLine("Secando auto");
  public void SeRecicla() => Console.WriteLine("Reciclando auto");
}