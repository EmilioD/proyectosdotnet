namespace ejercicio1;

interface IAlquilable
{
  void SeAlquilaA(Persona p);
  void SeDevuelveDe(Persona p);
}