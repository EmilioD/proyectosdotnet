namespace ejercicio1;

interface ILavable
{
  public void SeLava();
  public void SeSeca();
}