namespace ejercicio3;

interface IAlquilable
{
  void SeAlquilaA(Persona p);
  void SeDevuelveDe(Persona p);
}