namespace ejercicio3;

class Libro : IAlquilable, IReciclable
{
  public void SeAlquilaA(Persona p) => Console.WriteLine("Aquilando libro a persona");

  public void SeDevuelveDe(Persona p) => Console.WriteLine("Libro devuelvo por persona");
  public void SeRecicla() => Console.WriteLine("Reciclando libro");

}