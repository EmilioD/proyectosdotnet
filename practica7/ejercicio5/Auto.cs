namespace ejercicio3;

class Auto : IVendible, ILavable, IReciclable, IComercial, IImportante
{
  public void SeLava() => Console.WriteLine("Lavando auto");

  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo auto a persona");

  public void SeSeca() => Console.WriteLine("Secando auto");
  public void SeRecicla() => Console.WriteLine("Reciclando auto");

  void IComercial.Importa() => Console.WriteLine("Auto que se vende al exterior");

  void IImportante.Importa() => Console.WriteLine("Auto importante");

  public void Importa() => Console.WriteLine("Método Importar() de la clase Auto");
}