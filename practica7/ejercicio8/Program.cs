﻿// See https://aka.ms/new-console-template for more information
using System.Collections;


IEnumerable<int> Rango(int i, int j, int p)
{
  List<int> lista = new List<int> { };
  for (int x = i; x <= j; x += p) { lista.Add(x); }
  return lista;
}

IEnumerable<int> Potencias(int b, int k)
{
  List<int> lista = new List<int> { };
  for (int x = 1; x <= k; x++) { lista.Add((int)Math.Pow(b, x)); }
  return lista;
}
IEnumerable<int> DivisiblesPor(IEnumerable e, int i)
{
  List<int> lista = new List<int> { };
  foreach (int item in e)
  {
    if ( item % i == 0) lista.Add(item);
  }
  return lista;
}

IEnumerable rango = Rango(6, 30, 3);
IEnumerable potencias = Potencias(2, 10);
IEnumerable divisibles = DivisiblesPor(rango, 6);
foreach (int i in rango)
{
  Console.Write(i + " ");
}
Console.WriteLine();
foreach (int i in potencias)
{
  Console.Write(i + " ");
}
Console.WriteLine();
foreach (int i in divisibles)
{
  Console.Write(i + " ");
}
Console.WriteLine();
