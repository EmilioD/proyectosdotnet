namespace ejercicio3;

class Persona : IAtendible, IComercial, IImportante
{
  public void SeAtiende() => Console.WriteLine("Atendiendo persona");

  void IComercial.Importa() => Console.WriteLine("Persona vendiendo al exterior");

  void IImportante.Importa() => Console.WriteLine("Persona importante");

  public void Importa() => Console.WriteLine("Método Importar() de la clase Persona");
}