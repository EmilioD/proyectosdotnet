namespace ejercicio3;

interface ILavable
{
  public void SeLava();
  public void SeSeca();
}