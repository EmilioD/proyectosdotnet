namespace ejercicio3;

class ComparadorLongitudNombre : IComparer<INombrable>
{
  public int Compare(INombrable? x, INombrable? y)
  {
    if (x == null || y == null) return 1;
    return x.Nombre.Length.CompareTo(y?.Nombre.Length);
  }
}