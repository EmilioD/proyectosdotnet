namespace ejercicio3;

class Persona : IAtendible, IComercial, IImportante, INombrable
{
  public string? Nombre { get; set; }
  public void SeAtiende() => Console.WriteLine("Atendiendo persona");

  void IComercial.Importa() => Console.WriteLine("Persona vendiendo al exterior");

  void IImportante.Importa() => Console.WriteLine("Persona importante");

  public void Importa() => Console.WriteLine("Método Importar() de la clase Persona");

  public int CompareTo(INombrable? x)
  {
    if (x is Perro) return -1;
    return this.Nombre.CompareTo(x?.Nombre); 
  }

  public override string ToString() => $"{Nombre} es una persona";
}