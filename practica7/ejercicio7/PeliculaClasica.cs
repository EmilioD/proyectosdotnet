namespace ejercicio3;

class PeliculaClasica : Pelicula, IVendible
{
  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo película clásica a persona");

  public override void SeAlquilaA(Persona p)  => Console.WriteLine("Alquilando película clásica a persona");

  public override void SeDevuelveDe(Persona p)  => Console.WriteLine($"Película clásica devuelta de persona");
}