namespace ejercicio3;

class ComparadorRandom : IComparer<INombrable>
{
  public int Compare(INombrable? x, INombrable? y)
  {
    if (x == null || y == null) return 1;
    return (int) new Random().NextInt64();
  }
}