namespace ejercicio3;

class Persona : IAtendible, IComercial, IImportante, INombrable
{
  public string? Nombre { get; set; }
  public void SeAtiende() => Console.WriteLine("Atendiendo persona");

  void IComercial.Importa() => Console.WriteLine("Persona vendiendo al exterior");

  void IImportante.Importa() => Console.WriteLine("Persona importante");

  public void Importa() => Console.WriteLine("Método Importar() de la clase Persona");

  public int CompareTo(object? x)
  {

    int result = 0;
    if (x is Persona)
    {
      string? nombre = ((Persona)x).Nombre;
      result = this.Nombre.CompareTo(nombre); 
    }
    return result;
  }

  public override string ToString() => $"{Nombre} es una persona";
}