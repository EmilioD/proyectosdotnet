namespace ejercicio3;

class Perro : IVendible, IAtendible, ILavable, INombrable
{
  public string? Nombre { get; set; }
  public void SeAtiende() => Console.WriteLine("Atendiendo perro");
  public void SeVendeA(Persona p) => Console.WriteLine("Vendiendo perro a persona");

  public void SeLava() => Console.WriteLine("Lavando un perro");

  public void SeSeca() => Console.WriteLine("Secando perro");

  public int CompareTo(object? x)
  {
    int result = 0;
    if (x is Perro)
    {
      string? nombre = ((Perro)x).Nombre;
      result = this.Nombre.CompareTo(nombre); 
    }
    return result;
  }

  public override string ToString() => $"{Nombre} es un perro";

}