namespace ejercicio3;

interface INombrable : IComparable
{
  public string? Nombre { get; set; }

  public string ToString();
}