﻿// See https://aka.ms/new-console-template for more information
/* Codificar el método Swap que recibe 2 parámetros enteros e intercambia sus valores. El cambio
debe apreciarse en el método invocador. */

Console.WriteLine("Ingrese dos valores");
Console.Write("Valor a: ");
int a = int.Parse(Console.ReadLine());
Console.Write("Valor b: ");
int b = int.Parse(Console.ReadLine());

Swap(ref a, ref b);
Console.WriteLine("Valores intercambiados --> a: {0}, b: {1}", a, b);

void Swap(ref int a, ref int b)
{
  int aux = a;
  a = b;
  b = aux;
}