﻿/* 
Investigar sobre el tipo DateTime y usarlo para medir el tiempo de ejecución de los algoritmos
implementados en el ejercicio anterior. */
/* 
The DateTime value type represents dates and times with values ranging from 00:00:00 (midnight), January 1, 0001 Anno Domini (Common Era) through 11:59:59 P.M., December 31, 9999 A.D. (C.E.) in the Gregorian calendar.
Time values are measured in 100-nanosecond units called ticks. 
A particular date is the number of ticks since 12:00 midnight, January 1, 0001 A.D. (C.E.) in the GregorianCalendar calendar. 
The number excludes ticks that would be added by leap seconds. 
For example, a ticks value of 31241376000000000L represents the date Friday, January 01, 0100 12:00:00 midnight. 
A DateTime value is always expressed in the context of an explicit or default calendar.
*/

using System.Text;
string? st;
StringBuilder st_b;

Console.WriteLine("Ejemplo donde StringBuilder si aporta");
DateTime time = DateTime.Now;
DateTime time_final;

st = "Tiene un valor";
st = "Ahora tiene otro";
Console.WriteLine(st.StartsWith('A') ? "Ahora" : "Tiene");



time_final = DateTime.Now;
Console.WriteLine("Tiempo de ejecución usando String: " + (time_final - time));


time = DateTime.Now;

st_b = new StringBuilder("Tiene un valor");
st_b = new StringBuilder("Ahora tiene otro");
string? st_b_s = st_b.ToString();
Console.WriteLine(st_b_s.StartsWith('A') ? "Ahora" : "Tiene");



time_final = DateTime.Now;
time_final.ToString();
Console.WriteLine("Tiempo de ejecución usando StringBuilder: " + (time_final - time));


/////


Console.WriteLine("Ejemplo donde StringBuilder no aporta mucho");
time = DateTime.Now;

st = "Tiene un valor";
st = "Ahora tiene otro";
Console.WriteLine(st.StartsWith('A') ? "Ahora" : "Tiene");
st.Concat("Le sumo este string");
st.Concat(". Ahora le sumo este otro");
st.Concat(" y eeeeste");
for (int i = 0; i < st.Length; i++)
{
  st.Concat(i.ToString());
}



time_final = DateTime.Now;
Console.WriteLine("Tiempo de ejecución usando String: " + (time_final - time));


time = DateTime.Now;

st_b = new StringBuilder("Tiene un valor");
st_b = new StringBuilder("Ahora tiene otro");
st_b.Append("Le sumo este string");
st_b.Append(". Ahora le sumo este otro");
st_b.Append(" y eeeeste");
st_b.Remove(0, 3);
for (int i = 0; i < st.Length; i++)
{
  st_b.Append(i);
}


st_b_s = st_b.ToString();
Console.WriteLine(st_b_s.StartsWith('A') ? "Ahora" : "Tiene");





time_final = DateTime.Now;
time_final.ToString();
Console.WriteLine("Tiempo de ejecución usando StringBuilder: " + (time_final - time));




