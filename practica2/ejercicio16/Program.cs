﻿// See https://aka.ms/new-console-template for more information
/* Escribir una función (método int Fac(int n)) que calcule el factorial de un número n
pasado al programa como parámetro por la línea de comando
a) Definiendo una función no recursiva
b) Definiendo una función recursiva
c) idem a b) pero con expression-bodied methods (Tip: utilizar el operador condicional
ternario) */

Console.WriteLine("Recursiva: " + Fac(5));

int Fac(int n)
{
  if (n == 1)
  {
    return n;
  }
  else
    return Fac(n - 1) * n;
}

Console.WriteLine("Iterativa: " + FacRec(5));

int FacRec(int n)
{
  int fact= 1;
  for (int i = 1; i <= n; i++)
  {
    fact = fact * i;
  }
  return fact;
}

Console.WriteLine("Expression-bodied methods : " + FacExp(5));

int FacExp(int n) => n == 1 ? n : FacExp(n - 1) * n;