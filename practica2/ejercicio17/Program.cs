﻿// See https://aka.ms/new-console-template for more information
/* Ídem. al ejercicio 16.a) y 16.b) pero devolviendo el resultado en un parámetro de salida
void Fac(int n, out int f)*/
int f = 1;
Fac(5, out f);
Console.WriteLine("Recursiva: " + f);

void Fac(int n, out int f)
{
  if (n == 1)
  {
    f = n;
  }
  else{
    Fac(n - 1, out f);
    f *= n;
  }
}

FacRec(5, out f);
Console.WriteLine("Iterativa: " + f);

void FacRec(int n, out int f)
{
  f = 1;
  for (int i = 1; i <= n; i++)
  {
    f = f * i;
  }
}

f = 1;
Fac(5, out f);
Console.WriteLine("Expression-bodied methods : " + f);

int FacExp(int n, out int f) => n == 1 ? f = n : f = FacExp(n - 1, out f) * n;