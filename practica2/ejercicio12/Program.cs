﻿// See https://aka.ms/new-console-template for more information

using System.Text;

object[] v = new object[10];
v[0] = new StringBuilder("Net");
for (int i = 1; i < 10; i++)
{
  v[i] = v[i - 1];
}
(v[5] as StringBuilder).Insert(0, "Framework .");
foreach (StringBuilder s in v)
  Console.WriteLine(s);
//dibujar el estado de la pila y la mem. heap
//en este punto de la ejecución
v[5] = new StringBuilder("CSharp");
foreach (StringBuilder s in v)
  Console.WriteLine(s);
//dibujar el estado de la pila y la mem. heap
//en este punto de la ejecución


/*
En la línea 5 crea un arreglo de 10 elementos de tipo object en la heap
En la linea 6, al primer elemento del arreglo de object, le instancia un Stringbuilder
es decir, le asigna un objecto StringBuilder en la heap y asigna la referencia en el arreglo v
En el for, vincula esa referencia al resto de elementos del arreglo v
En la linea 11, al elemento en la posición 5 del arreglo v, le inserta el texto "Framework ." al inicio
esto en si, lo ven reflajdas las demás referencias ya que apuntan al mismo sector en la heap
En la linea 16, genera un nuevo StringBuilder en la heap, por lo que ese índice ya no apunta al anterior
sino que genera un nuevo elemento la heap. Esto hace que las demás referencias queden inmutables.
*/