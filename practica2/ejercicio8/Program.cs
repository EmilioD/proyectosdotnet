﻿// See https://aka.ms/new-console-template for more information
/* 
  Escribir un programa que reciba una lista de nombres como parámetro e imprima por consola un
  saludo personalizado para cada uno de ellos.
  a) utilizando la sentencia for
  b) utilizando la sentencia foreach
*/

void saluteName(string? st){
  switch(st) 
  {
    case "":
      Console.WriteLine("Buen día mundo");  
      break;
    case "Juan":
      Console.WriteLine("Hola amigo!");
      break;
    case "Maria":
      Console.WriteLine("Buen día señora");
      break;
    case "Alberto":
      Console.WriteLine("Hola Alberto");
      break;
    default:
      Console.WriteLine("Buen día " + st);
      break;
  }
}

if (args != null){
  for (int i = 0; i < args.Length; i++)
  {
    saluteName(args[i]);
  }

  foreach (string item in args)
  {
    saluteName(item);
  }
}