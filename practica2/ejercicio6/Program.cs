﻿// See https://aka.ms/new-console-template for more information
/* Determinar qué hace el siguiente programa y explicar qué sucede si no se pasan parámetros
cuando se invoca desde la línea de comandos. */

Console.WriteLine("¡Hola {0}!", args[0]);

/* Dentro del string llama al argumento que se coloca luego de la coma
Esta manera de llamar a los argumentos es por orden
Si no se pasan argumentos va a intentar acceder a un vector sin elementos, es decir, Error en tiempo de ejecución
Error de índices, quiere acceder con un índice donde no hay elementos, como si quisiera hacer vector[100], donde el vector no tiene 100 elementos */