﻿// See https://aka.ms/new-console-template for more information
/* Codificar el método Imprimir para que el siguiente código produzca la salida por consola que
se observa. Considerar que el usuario del método Imprimir podría querer más adelante
imprimir otros datos, posiblemente de otros tipos pasando una cantidad distinta de parámetros
cada vez que invoque el método.
TIP: usar params */

Imprimir(1, "casa", 'A', 3.4, DayOfWeek.Saturday);
Imprimir(1, 2, "tres");
Imprimir();
Imprimir("--------");

void Imprimir(params dynamic[] vector)
{
  foreach (var item in vector)
  {
    Console.Write("{0} ", item);
  }
  Console.WriteLine("");
}