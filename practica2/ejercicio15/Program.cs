﻿// See https://aka.ms/new-console-template for more information
/* Escribir una función (método int Fib(int n)) que calcule el término n de la serie de
Fibonacci.
Fib(n) = 1, si n <=2
Fib(n) = Fib(n-1) + Fib(n-2), si n > 2 */

int number;
Console.WriteLine("Enter the number of elements:");
number = int.Parse(Console.ReadLine());

Console.Write(Fib(number) + " ");

int Fib(int n){
  if ((n == 0) || (n == 1))
  {
    return n;
  }
  else
    return Fib(n - 1) + Fib(n - 2);
}