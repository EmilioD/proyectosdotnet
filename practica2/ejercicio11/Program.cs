﻿// See https://aka.ms/new-console-template for more information
/* 
¿Para qué sirve el método Split de la clase string? Usarlo para escribir en la consola todas
las palabras (una por línea) de una frase ingresada por consola por el usuario. 
*/

/* 
Returns a string array that contains the substrings in this instance that are delimited by 
elements of a specified string or Unicode character array.
The String.Split method separates a single string into multiple strings. 
Overloads of the method allow you to specify multiple delimiters, t
o limit the number of substrings that the method extracts, 
to trim white space from substrings, and to specify whether empty strings 
(which occur when delimiters are adjacent) are included among the returned strings. 
*/

string? st;
string?[] splits;

Console.Write("Ingrese una frase: ");
st = Console.ReadLine();
splits = st.Split();
Console.WriteLine("Palabras divididas: ");
foreach (var item in splits)
{
  Console.WriteLine(item);
}
