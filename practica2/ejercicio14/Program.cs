﻿// See https://aka.ms/new-console-template for more information
/* Implementar un programa que muestre todos los números primos entre 1 y un número natural
dado (pasado al programa como argumento por la línea de comandos). Definir el método bool
EsPrimo(int n) que devuelve true sólo si n es primo. Esta función debe comprobar si n es
divisible por algún número entero entre 2 y la raíz cuadrada de n. (Nota: Math.Sqrt(d)
devuelve la raíz cuadrada de d) */

int num = int.Parse(args[0]);

Console.WriteLine(EsPrimo(num) ? "Es primo" : "No es primo");

bool EsPrimo(int n){
  for (int i = 2; i < Math.Sqrt(n); i++)
  {
    if (n % i == 0)
    {
      return true;
    }
  }
  return false;
}