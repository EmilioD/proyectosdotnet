﻿// See https://aka.ms/new-console-template for more information
/* Investigar acerca de la clase StringBuilder del espacio de nombre System.Text ¿En qué
circunstancias es preferible utilizar StringBuilder en lugar de utilizar string? Implementar
un caso de ejemplo en el que el rendimiento sea claramente superior utilizando
StringBuilder en lugar de string y otro en el que no. */
/* Consider using the String class under these conditions:

    When the number of changes that your code will make to a string is small. In these cases, StringBuilder might offer negligible or no performance improvement over String.

    When you are performing a fixed number of concatenation operations, particularly with string literals. In this case, the compiler might combine the concatenation operations into a single operation.

    When you have to perform extensive search operations while you are building your string. The StringBuilder class lacks search methods such as IndexOf or StartsWith. You'll have to convert the StringBuilder object to a String for these operations, and this can negate the performance benefit from using StringBuilder. For more information, see the Searching the text in a StringBuilder object section.

Consider using the StringBuilder class under these conditions:

    When you expect your code to make an unknown number of changes to a string at design time (for example, when you are using a loop to concatenate a random number of strings that contain user input).

    When you expect your code to make a significant number of changes to a string.
 */

using System.Text;
string? st;
StringBuilder st_b;

Console.WriteLine("Ejemplo donde StringBuilder si aporta");
DateTime time = DateTime.Now;
DateTime time_final;

st = "Tiene un valor";
st = "Ahora tiene otro";
Console.WriteLine(st.StartsWith('A') ? "Ahora" : "Tiene");



time_final = DateTime.Now;
Console.WriteLine("Tiempo de ejecución usando String: " + (time_final - time));


time = DateTime.Now;

st_b = new StringBuilder("Tiene un valor");
st_b = new StringBuilder("Ahora tiene otro");
string? st_b_s = st_b.ToString();
Console.WriteLine(st_b_s.StartsWith('A') ? "Ahora" : "Tiene");



time_final = DateTime.Now;
time_final.ToString();
Console.WriteLine("Tiempo de ejecución usando StringBuilder: " + (time_final - time));


/////


Console.WriteLine("Ejemplo donde StringBuilder no aporta mucho");
time = DateTime.Now;

st = "Tiene un valor";
st = "Ahora tiene otro";
Console.WriteLine(st.StartsWith('A') ? "Ahora" : "Tiene");
st.Concat("Le sumo este string");
st.Concat(". Ahora le sumo este otro");
st.Concat(" y eeeeste");
for (int i = 0; i < st.Length; i++)
{
  st.Concat(i.ToString());
}



time_final = DateTime.Now;
Console.WriteLine("Tiempo de ejecución usando String: " + (time_final - time));


time = DateTime.Now;

st_b = new StringBuilder("Tiene un valor");
st_b = new StringBuilder("Ahora tiene otro");
st_b.Append("Le sumo este string");
st_b.Append(". Ahora le sumo este otro");
st_b.Append(" y eeeeste");
st_b.Remove(0, 3);
for (int i = 0; i < st.Length; i++)
{
  st_b.Append(i);
}


st_b_s = st_b.ToString();
Console.WriteLine(st_b_s.StartsWith('A') ? "Ahora" : "Tiene");





time_final = DateTime.Now;
time_final.ToString();
Console.WriteLine("Tiempo de ejecución usando StringBuilder: " + (time_final - time));




